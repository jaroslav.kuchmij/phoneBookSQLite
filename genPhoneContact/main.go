package main

import (
	"fmt"
	"os"
	"runtime"
	"time"
	"gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git/genPhoneContact/settings"
	"gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git/genPhoneContact/models"
)

var count1  = 0//счетчик каждой записи в файл

func main() {
	//Задействуем 4 ядра процессора
	runtime.GOMAXPROCS(4)
	var count = settings.Batch

	for count <= settings.MaxNumber{
		//создание горутины для функции genContacts и передача счетчика
		go genContacts( count)
		count = count + settings.Batch
	}

	//время ожидание перед закрытием функции
	time.Sleep(settings.TimeWait * time.Minute)

}

//генерация контактов
func genContacts(count int)  {
	var interval = count - settings.Batch
	for interval < count {
		if count1 == settings.MaxNumber {
			return
		}
		//формирование номера контакта
		res := fmt.Sprintf(settings.CodeNumber + "%09d", interval)
		//формирование контакта
		contact := models.Contact{Number: res, Name: "Name", Street: "Street", NumberHouse: "House", NumberFlat: "Flat"}
		//запись данных
		setNumber(contact)
		interval++
		count1++
	}

}

//Запись номера в справочник
func setNumber(contact models.Contact) {
	//открытие/создание файла
	f, err := os.OpenFile(settings.NameFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	//запись в файл
	if _, err = f.WriteString(contact.Number + ";"+contact.Name + ";"+contact.Street + ";"+contact.NumberHouse + ";"+contact.NumberFlat +"|"); err != nil {
		panic(err)
	}
	return
}