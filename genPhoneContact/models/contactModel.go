package models

type Contact struct {
	Number string
	Name string
	Street string
	NumberHouse string
	NumberFlat string
}