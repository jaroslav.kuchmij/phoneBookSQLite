package models

type Contact struct {
	Number string `csv:"contact_number"`
	Name string `csv:"contact_name"`
	Street string `csv:"contact_street"`
	NumberHouse string `csv:"contact_numberHouse"`
	NumberFlat string `csv:"contact_numberFlat"`
}