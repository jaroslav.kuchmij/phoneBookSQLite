package main

import (
	"database/sql"
	"log"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"os"
	"github.com/joho/sqltocsv"
	"gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git/unloadingInManyFiles/settings"
)

var (DB *sql.DB
	nameCSV string
	countStep = 1
)


func main()  {
	//УДаление предыдущей БД
	os.Remove("./foo.db")

	var err error
	//Открытие БД
	DB, err = sql.Open("sqlite3", "./foo.db")
	if err != nil {
		log.Fatal(err)
	}
	defer DB.Close()

	//Создание таблиц в БД
	err = createTable()
	if err != nil {
		log.Printf("error in createTable: ", err)
		return
	}


	//Запись в БД данных из полученного файла
	err = insertTable()
	if err != nil {
		log.Printf("error in insertTable: ", err)
		return
	}

	//Выгрузка данных в файл CSV
	err = unloadingData()
	if err != nil {
		log.Printf("error in unloadingData: ", err)
		return
	}

}

//Создание талиц
func createTable() error{
	//Команда по созданию таблицы в БД
	sqlStmt := `
	CREATE TABLE phonebook (id integer not null primary key ,
	number text ,
	name text,
	street text,
	numberHouse text,
	numberFlat);

`	//Применение команды
	_, err := DB.Exec(sqlStmt)
	if err != nil {
		return err
	}
	return nil
}

//Запись данных в таблицу БД
func insertTable() error  {
	var count = 1

	//начало транзакции
	tx, err := DB.Begin()
	if err != nil {
		return err
	}

	//Подготовка SQL запроса для выполнения в транзакции
	stmt, err := tx.Prepare("INSERT INTO phonebook(id, number, name, street, numberHouse, numberFlat) values(?, ?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	for count <= settings.QuantityRecords {
		//формирование номера контакта
		res := fmt.Sprintf(settings.CodeNumber + "%09d", count)
		_, err = stmt.Exec(count, res, "Name", "Street", "House", "Flat")
		if err != nil {
			return err
		}
		count++
	}

	tx.Commit()
	return nil
}

//Выгрузка данных из таблицы в БД в файл CSV
func unloadingData() error  {
	//генерация имени/пути к файлу CSV
	var count = 1
	var id = settings.BatchSize
	for count <= settings.MaxCountFile {
		nameCSV = fmt.Sprintf("unloading/%d.csv", count)
		//Создание/Открытие фала
		contactsFile, err := os.OpenFile(nameCSV, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			return err
		}
		defer contactsFile.Close()

		if _, err := contactsFile.Seek(0, 0); err != nil { // Go to the start of the file
			return err
		}

		//выполнение запроса и запись полученных данных в переменную
		rows, err := DB.Query(fmt.Sprintf("select number, name, street, numberHouse, numberFlat from phonebook where id >= %d and id < %d ORDER BY id", countStep, id))
		if err != nil {
			return err
		}
		defer rows.Close()

		err = sqltocsv.WriteFile(nameCSV, rows)
		if err != nil {
			return err
		}

		countStep = countStep + settings.BatchSize
		id = id + settings.BatchSize
		count++
	}


	return nil
}