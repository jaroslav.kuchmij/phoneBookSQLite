# Project "SQLite"

### В этом проекте выполняется:
1. Генерация и Загрузка 100 000 000 записей в SQLite
3. Генерация файла(-ов) формата CSV

### Запуск проекта:
Скачать проект 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git
</pre>


#### Generation CSV 

1. Перейти в пакет:
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git/unloadingInManyFiles/settings
и указать настройки для генерации CSV файла

2. Перейти в пакет: 
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookSQLite.git/unloadingInManyFiles
и запустить команду:
<pre>
go run main.go
</pre>


**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)